![banner](https://gitlab.com/programacion-web---i-sem-2019/ufps_css/-/raw/master/css/1/portada-web.png)
# Titulo del proyecto
#### Curriculum vitae
## indice
1. [Caracteristicas del proyecto](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologias](#tecnologias)
4. [IDE](#ide)
5. [Instalacion](#instalacion)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institucion Academica](#institucion-academica)
#### caracteristicas
- Uso de CSS recomendado: [ver](https://gitlab.com/Andrey1304/1151804_hojadevida_css/-/tree/master/css)
#### Contenido del proyecto
- [index.html](https://gitlab.com/Andrey1304/1151804_hojadevida_css/-/blob/master/index.html)
#### Tecnologias
- [HTML](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)
- [CSS](https://developer.mozilla.org/es/docs/Web/CSS)
#### IDE
- [Sublime text](https://www.sublimetext.com/)
#### Instalacion
1. local
    - Descarga el repositorio ubicado en [Descargar](https://gitlab.com/Andrey1304/1151804_hojadevida_css)
    - Abrir el archivo index.html desde el navegador predeterminado

2. Hosting GitLab
    - Realizando un fork.
#### Demo
- Se puede visualizar la version demo [aqui](https://andrey1304.gitlab.io/1151804_hojadevida_css/)
#### Autor(es)
- Realizado por: Brayam Andrey Oliveros Rivera
#### Institucion Academica
- Universidad Francisco de Paula Santander
